//
//  StartViewModelProtocol.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 11/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

protocol StartViewModelProtocol {
    // MARK: Properties
    var classButtonTitle: String { get set }
    
    var isLoadingSuccess: Bool { get set }
    var errorData: (title: String, msg: String) { get set }
    
    var hsClassNames: [String] { get set }
    
    // MARK: Methods
    func updateData()
}
