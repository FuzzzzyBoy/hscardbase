//
//  Result.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 02/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

// Either a Value value or an ErrorType
enum Result<Value> {
    // Success wraps a Value value
    case success(Value)
    
    // Failure wraps an ErrorType
    case failure(Error)
    
    // Construct a result from a `throws` function
    public init(_ capturing: () throws -> Value) {
        do {
            self = .success(try capturing())
        } catch {
            self = .failure(error)
        }
    }
    
    // Adapter method used to convert a Result to a value while throwing on error.
    public func unwrap() throws -> Value {
        switch self {
        case .success(let v): return v
        case .failure(let e): throw e
        }
    }
    
    // Convenience tester/getter for the value
    public var value: Value? {
        switch self {
        case .success(let v): return v
        case .failure: return nil
        }
    }
    
    // Convenience tester/getter for the error
    public var error: Error? {
        switch self {
        case .success: return nil
        case .failure(let e): return e
        }
    }
}
