//
//  AppDelegate.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 23/05/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let defaultsProvider = DefaultsProvider()
    let dataProvider = DataProvider()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let navigationVC = window?.rootViewController as? UINavigationController {
            if let startVC = navigationVC.viewControllers.first as? StartViewController {
                let startVM = StartViewModel(with: dataProvider, and: defaultsProvider)
                startVC.viewModel = startVM
            }
        }
        
        return true
    }
}
