//
//  URLSessionProtocol.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 06/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

public typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void

public protocol SessionProtocol {
    func getData(with request: URLRequest, completion: @escaping DataTaskResult)
}

extension URLSession: SessionProtocol {
    public func getData(with request: URLRequest, completion: @escaping DataTaskResult) {
        let task = self.dataTask(with: request) { (data, response, error) in
            completion(data, response, error)
        }
        task.resume()
    }
}
