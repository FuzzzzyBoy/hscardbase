//
//  CardCellViewModel.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 17/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation
import UIKit

class CardCellViewModel: NSObject {
    
    var name: String {
        return card.name
    }
    var text: String {
        return card.text
    }
    dynamic var image = #imageLiteral(resourceName: "placeholder")
    
    func downloadImage() {
        if let url = card.imgURL {
            downloadTask = loadImage(url: url)
        }
    }
    
    init(with card: HSCard) {
        self.card = card
    }
    
    private let card: HSCard!
    private var downloadTask: URLSessionDownloadTask?
    
    private func loadImage(url: URL) -> URLSessionDownloadTask {
        let session = URLSession.shared
        let downloadTask = session.downloadTask(with: url) { [weak self] url, _, error in
            if error == nil,
                let url = url,
                let data = try? Data(contentsOf: url),
                let image = UIImage(data: data) {
                self?.image = image
            }
        }
        downloadTask.resume()
        return downloadTask
    }
    
    deinit {
        downloadTask?.cancel()
        downloadTask = nil
    }
}
