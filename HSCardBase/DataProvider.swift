//
//  DataProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 29/05/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

class DataProvider: DataProviderProtocol {
    
    // MARK: - API Provider object
    private let requestProvider: RequestProviderProtocol
    
    // MARK: - Private variables
    // MARK: result of previous request
    private var info: InfoData?
    
    // MARK: Initializer
    init(requestProvider: RequestProviderProtocol = RequestProvider()) {
        self.requestProvider = requestProvider
    }
    
    // MARK: - Public methods
    func getClasses(completion: @escaping (Result<Classes>) -> Void) {
        
        if let info = info {
            parseHsClasses(from: info, completion: { (result) in
                completion(result)
            })
        } else {
            getInfo { [unowned self] result in
                do {
                    let info = try result.unwrap()
                    self.parseHsClasses(from: info, completion: { (result) in
                        completion(result)
                    })
                } catch {
                    completion(Result({ throw error }))
                }
            }
        }
    }
    
    func getCards(byClassName className: String,
                  completion: @escaping (Result<Cards>) -> Void) {
        requestProvider.getCards(byClassName: className) { result in
            
            do {
                let cardsData = try result.unwrap()
                var cards = [HSCard]()
                for data in cardsData {
                    if let card = HSCard(parameters: data) {
                        cards.append(card)
                    }
                }
                
                if cards.isEmpty {
                    completion(Result({ throw LoadingError.invalidData }))
                } else {
                    completion(Result({ return cards }))
                }
            } catch {
                completion(Result({ throw error}))
            }
        }
    }
    
    // MARK: - Private methods
    // MARK: Get info object from data provider
    private func getInfo(completion: @escaping (RequestResult<InfoData>) -> Void) {
        requestProvider.getInfo { [unowned self] (result) in
            do {
                let json = try result.unwrap()
                self.info = json
                completion(Result({return json}))
            } catch {
                completion(Result { throw error})
            }
        }
    }
    
    // MARK: Parse hsClasses from json object
    func parseHsClasses(from data: [String: Any],
                        completion: @escaping (Result<Classes>) -> Void) {
        
        completion(Result({
            guard var names = data["classes"] as? [String] else {
                throw LoadingError.invalidData
            }
            
            // workaround for issue: server return Death Knight as first element of classes,
            // there is no cards for this class
            // except first element
            if names.first == "Death Knight" {
                names.removeFirst()
            }
            
            return names.map { HSClass(name: $0) }
        }))
    }
}
