//
//  DefaultsProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

private enum Keys {
    static let currentClassName = "currentClassName"
}

class DefaultsProvider: DefaultsProviderProtocol {
    
    // MARK: - DefaultsProviderProtocol property
    private let defaults: DefaultsProtocol
    
    // MARK: - Init
    init(defaults: DefaultsProtocol = UserDefaults.standard) {
        self.defaults = defaults
    }
    
    // MARK: - Public properties
    var currentClassName: String? {
        get {
            return defaults.string(forKey: Keys.currentClassName)
        }
        
        set {
            if let value = newValue {
                defaults.set(value, forKey: Keys.currentClassName)
            }
        }
    }
}
