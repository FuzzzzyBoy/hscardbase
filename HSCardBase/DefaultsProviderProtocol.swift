//
//  DefaultsProviderProtocol.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

protocol DefaultsProviderProtocol {
    var currentClassName: String? { get set }
}
