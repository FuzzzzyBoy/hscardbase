//
//  RequestProviderProtocol.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 06/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

typealias ResponseData = [String: Any]
typealias RequestResult<ResponseData> = Result<ResponseData>

typealias CardsData = [[String: Any]]
typealias RequestCardsResult = Result<CardsData>

typealias InfoData = [String: [String]]

protocol RequestProviderProtocol {
    func getInfo(completion: @escaping (RequestResult<InfoData>) -> Void)
    func getCards(byClassName className: String, completion: @escaping (RequestCardsResult) -> Void)
}
