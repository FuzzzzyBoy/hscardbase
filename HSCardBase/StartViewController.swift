//
//  StartViewController.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 29/05/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    let cardCellIdentifier = "CardCell"
    
    // MARK: - @IBOutlets
    @IBOutlet weak var classButton: UIButton!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var cardsTable: UITableView!
    
    // MARK: @IBACtion
    @IBAction func classButtonPressed(_ sender: UIButton) {
        chooseClassSwitchVisible()
    }
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        chooseClassSwitchVisible()
    }
    @IBAction func saveButtonPressed(_ sender: UIBarButtonItem) {
        chooseClassSwitchVisible()
        if !viewModel.hsClassNames.isEmpty {
            viewModel.classButtonTitle =
                viewModel.hsClassNames[picker.selectedRow(inComponent: 0)]
        }
    }
    
    // MARK: - view model
    var viewModel: StartViewModel! {
        didSet {
            beginObserving()
        }
    }
    
    // MARK: - array of variables for store keyPath
    private let keyPathes = [#keyPath(viewModel.classButtonTitle),
                             #keyPath(viewModel.isLoadingSuccess),
                             #keyPath(viewModel.rowsCount)]
    
    // MARK: - UIView lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.updateData()
        
        // Picker View
        picker.dataSource = self
        picker.delegate = self
        
        // Table View
        let cell = UINib(nibName: cardCellIdentifier, bundle: nil)
        cardsTable.register(cell, forCellReuseIdentifier: cardCellIdentifier)
        cardsTable.rowHeight = 165
    }
    
    deinit {
        finishObserving()
    }
    
    // MARK: - Alert
    func showAlert(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Updata
    func updateView() {
        DispatchQueue.main.async { [unowned self] in
            self.classButton.setTitle(self.viewModel.classButtonTitle, for: .normal)
            if !self.viewModel.isLoadingSuccess {
                self.showAlert(title: self.viewModel.errorData.title, msg: self.viewModel.errorData.msg)
            }
            self.picker.reloadAllComponents()
            self.cardsTable.reloadData()
        }
    }
    
    // MARK: - KVO
    private func beginObserving() {
        for keyPath in keyPathes {
            addObserver(self, forKeyPath: keyPath, options: [], context: nil)
        }
    }
    
    private func finishObserving() {
        for keyPath in keyPathes {
            removeObserver(self, forKeyPath: keyPath)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        updateView()
    }
    
    // MARK: - Private methods
    func chooseClassSwitchVisible() {
        picker.isHidden = !picker.isHidden
        toolBar.isHidden = !toolBar.isHidden
    }
}

// MARK: UIPickerViewDataSource
extension StartViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.hsClassNames.count
    }
}

// MARK: UIPickerViewDelegate
extension StartViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.hsClassNames[row]
    }
}

// MARK: UITableViewDataSource
extension StartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier: cardCellIdentifier,
                                          for: indexPath) as? CardCell else {
            return UITableViewCell()
        }
        cell.viewModel = viewModel.viewModelForCell(at: indexPath.row)
        return cell
    }
}

extension StartViewController: UITableViewDelegate {
}
