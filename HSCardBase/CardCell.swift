//
//  CardCell.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 17/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    
    let keyPath = #keyPath(viewModel.image)
    
    var viewModel: CardCellViewModel! {
        didSet {
            addObserver(self,
                        forKeyPath: keyPath,
                        options: [],
                        context: nil)
            
            name.text = viewModel.name
            name.sizeToFit()
            cardImage.image = viewModel.image
            viewModel.downloadImage()
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        DispatchQueue.main.async { [unowned self] in
            self.cardImage.image = self.viewModel.image
        }
    }
    
    override func prepareForReuse() {
        removeObserver(self, forKeyPath: keyPath)
    }
    
    deinit {
        removeObserver(self, forKeyPath: keyPath)
    }
}
