//
//  HSCard.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 16/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

struct HSCard {
    let name: String
    let cardId: String
    let text: String
    let imgURL: URL?
    
    init?(parameters: [String: Any]) {
        guard let name = parameters["name"] as? String else { return nil }
        guard let cardId = parameters["cardId"] as? String else { return nil }
        guard let text = parameters["text"] as? String else { return nil }
        guard let stringUrl = parameters["img"] as? String else { return nil }
        
        self.name = name
        self.cardId = cardId
        self.text = text
        
//        let startIndex = stringUrl.characters.startIndex
//        let index = stringUrl.index(startIndex, offsetBy: 4)
//        stringUrl.insert("s", at: index)
        self.imgURL = URL(string: stringUrl)
    }
}

extension HSCard: Equatable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    static func == (lhs: HSCard, rhs: HSCard) -> Bool {
        return lhs.cardId == rhs.cardId
    }
}
