//
//  HSClass.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 05/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

struct HSClass {
    let name: String
    
    init(name: String) {
        self.name = name
    }
}

extension HSClass: Equatable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    static func == (lhs: HSClass, rhs: HSClass) -> Bool {
        return lhs.name == rhs.name
    }
}
