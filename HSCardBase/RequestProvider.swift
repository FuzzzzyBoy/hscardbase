//
//  RequestProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 29/05/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

public class RequestProvider: RequestProviderProtocol {
    
    // MARK: - Private properties
    // Endpoint
    private let endpoint = "https://omgvamp-hearthstone-v1.p.mashape.com/"
    
    // Headers
    private let defaultHeader = (
        field: "X-Mashape-Key",
        value: "QmjlAjOjNwmshuH5cXVicxWjM627p1FSosljsnRBZALaukcOm3")
    
    // MARK: - URLSessionProtocol property
    private let session: SessionProtocol
    
    // MARK: - Init
    init(session: SessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    // MARK: - Public methods
    
    // MARK: RequestProviderProtocol
    // MARK: getInfo
    // make get request that returns a list of
    // current patch, classes, sets, types, factions, qualities, races and locales.
    func getInfo(completion: @escaping (RequestResult<InfoData>) -> Void) {
        
        guard var request = createRequest(withAdditional: "info") else { return }
        
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        getData(with: request) { (result) in
            do {
                let data = try result.unwrap()
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                var tempJson = jsonData as? [String: Any] else {
                        completion(Result({ throw LoadingError.invalidData }))
                        return
                }
                
                // workaround for issue: Server return data with type [String: [String]]
                // except key: "patch" with value type String
                tempJson.removeValue(forKey: "patch")
                
                guard let json = tempJson as? InfoData else {
                    completion(Result({ throw LoadingError.invalidData }))
                    return
                }
                
                completion(Result({ return json }))
                
            } catch {
                completion(Result { throw error})
            }
        }
    }
    
    // MARK: getCardsByClass
    func getCards(byClassName className: String,
                  completion: @escaping (RequestCardsResult) -> Void) {
        guard let request = createRequest(withAdditional: "cards/classes/" + className) else { return }
        
        getData(with: request) { (result) in
            do {
                let data = try result.unwrap()
                
                guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {
                    completion(Result({ throw LoadingError.invalidData }))
                    return
                }
                
                guard let json = jsonData as? CardsData else {
                    completion(Result({ throw LoadingError.invalidData }))
                    return
                }
                
                completion(Result({ return json }))
                
            } catch {
                completion(Result { throw error})
            }
        }
    }
    
    // MARK: - Private methods
    // MARK: create request
    private func createRequest(withAdditional addString: String) -> URLRequest? {
        let urlString = endpoint + addString
        guard let requestUrl = URL(string: urlString) else { return nil }
        
        var request = URLRequest(url: requestUrl)
        
        request.setValue(defaultHeader.value, forHTTPHeaderField: defaultHeader.field)
        
        return request
    }
    
    // MARK: get data from request with handle error
    private func getData(with request: URLRequest, completion: @escaping (Result<Data>) -> Void) {
        self.session.getData(with: request) { (data, response, error) in
            if let error = error {
                completion(Result({ throw LoadingError.requestError(error.localizedDescription) }))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(Result({ throw LoadingError.responseInvalid }))
                return
            }
            
            guard 200 ... 299 ~= httpResponse.statusCode else {
                completion(Result({ throw LoadingError.responseStatus(httpResponse.statusCode) }))
                return
            }
            
            guard let data = data else {
                completion(Result({ throw LoadingError.dataEmpty }))
                return
            }
            
            completion(Result({ return data }))
        }
    }
}
