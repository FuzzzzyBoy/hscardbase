//
//  Equatable.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 07/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

extension Dictionary where Key == String, Value == [String] {
    func isEqual(_ rhs: InfoData) -> Bool {
        
        guard self.count == rhs.count else { return false }
        
        for (key, value) in self {
            guard let rhsValue = rhs[key] else { return false }
            
            if value != rhsValue {
                return false
            }
        }
        
        return true
    }
}
