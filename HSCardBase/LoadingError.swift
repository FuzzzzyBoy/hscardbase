//
//  LoadingError.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 05/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

public enum LoadingError: Error {
    case dataEmpty
    case invalidData
    case responseInvalid
    case responseStatus(Int)
    case requestError(String)
}

extension LoadingError: Equatable {}

public func == (lhs: LoadingError, rhs: LoadingError) -> Bool {
    switch (lhs, rhs) {
    case (.dataEmpty, .dataEmpty),
         (.invalidData, .invalidData),
         (.responseInvalid, .responseInvalid),
         (.requestError, .requestError):
        return true
    case (let .responseStatus(st1), let .responseStatus(st2)):
        guard st1 == st2 else { return false }
        return true
    default:
        return false
    }
}
