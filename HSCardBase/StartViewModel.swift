//
//  StartViewModel.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 29/05/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

class StartViewModel: NSObject {
    
    // MARK: - Public interface
    // MARK: Properties
    dynamic var classButtonTitle = "None" {
        didSet {
            defaultsProvider.currentClassName = classButtonTitle
            updateCardsList()
        }
    }
    
    dynamic var isLoadingSuccess: Bool = true
    var errorData = (title: "", msg: "") {
        didSet {
            isLoadingSuccess = false
        }
    }
    
    // MARK: Picker View
    dynamic var hsClassNames = [String]()
    
    // MARK: Table view
    dynamic var rowsCount = 0
    
    // MARK: Methods
    // MARK: Update data
    func updateData() {
        if let currentClassName = defaultsProvider.currentClassName, !hsClassNames.isEmpty {
            classButtonTitle = currentClassName
        } else {
            dataProvider.getClasses(completion: { [unowned self] result in
                
                do {
                    let hsClasses = try result.unwrap()
                    
                    self.hsClassNames = hsClasses.map({$0.name})
                    
                    if let name = self.hsClassNames.first {
                        self.classButtonTitle = name
                    }
                    
                } catch {
                    if let error = error as? LoadingError {
                        self.set(error: error)
                    }
                }
            })
        }
    }
    
    // MARK: viewModel for cell
    func viewModelForCell(at index: Int) -> CardCellViewModel {
        return CardCellViewModel(with: cardsList[index])
    }
    
    // MARK: - Private properties
    private var cardsList = Cards()
    
    // MARK: - Private methods
    private func updateCardsList() {
        if classButtonTitle != "None" {
            dataProvider.getCards(byClassName: classButtonTitle, completion: { [unowned self] result in
                do {
                    let cards = try result.unwrap()
                    self.cardsList = cards
                    self.rowsCount = cards.count
                } catch {
                    if let error = error as? LoadingError {
                        self.set(error: error)
                    }
                }
            })
        }
    }
    
    // MARK: - Dependencies
    private let dataProvider: DataProviderProtocol
    private var defaultsProvider: DefaultsProviderProtocol
    
    init(with dataProvider: DataProviderProtocol, and defaultsProvider: DefaultsProviderProtocol) {
        self.dataProvider = dataProvider
        self.defaultsProvider = defaultsProvider
        
        super.init()
    }
        
    // MARK: Show alert
    func set(error: LoadingError) {
        switch error {
        case .responseInvalid, .responseStatus:
            errorData = ("Network issue", "Check network connection or try to connect later")
        case .requestError(let text):
            errorData = ("Network issue", text)
        case .dataEmpty, .invalidData:
            errorData = ("Data issue", "Can't read data. Try update app and repeat")
        }
    }
}
