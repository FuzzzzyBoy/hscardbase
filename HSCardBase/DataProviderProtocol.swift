//
//  DataProviderProtocol.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

typealias Classes = [HSClass]
typealias Cards = [HSCard]

protocol DataProviderProtocol {
    func getClasses(completion: @escaping (Result<Classes>) -> Void)
    func getCards(byClassName className: String, completion: @escaping (Result<Cards>) -> Void)
}
