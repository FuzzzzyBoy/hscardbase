//
//  StartViewModelTests.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

class StartViewModelTests: XCTestCase {
    
    var subject: StartViewModel!
    var defaultsProvider: MockDefaultsProvider!
    var dataProvider: MockDataProvider!
    
    let timeout = 10.0
    
    let hsClasses = [HSClass(name: "Mage"),
                     HSClass(name: "Warrior")]
    
    let hsClassNames = ["Mage", "Warrior"]
    
    let defaultClassButtonTitle = "None"
    
    let validJsonData = [
        [
            "cardId": "AT_071e",
            "name": "Alexstrasza's Boon",
            "text": "+1 Attack and <b>Charge</b>."
        ],
        [
            "cardId": "GVG_086e",
            "name": "Armor Plated",
            "text": "Increased Attack."
        ],
        [
            "cardId": "GVG_056t",
            "name": "Burrowing Mine",
            "text": "When you draw this, it explodes. You take 10 damage and draw a card.",
            "img": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/GVG_056t.png"
        ]]
    
    lazy var cards: Cards = {
        var resultArray = Cards()
        for data in self.validJsonData {
            if let card = HSCard(parameters: data) {
                resultArray.append(card)
            }
        }
        return resultArray
    }()
    
    override func setUp() {
        super.setUp()
        
        defaultsProvider = MockDefaultsProvider()
        dataProvider = MockDataProvider()
        subject = StartViewModel(with: dataProvider, and: defaultsProvider)
    }
    
    // test for errorData
    private let errorData: [(title: String, msg: String)] = [
        ("Network issue", "Check network connection or try to connect later"),
        ("Network issue", ""),
        ("Data issue", "Can't read data. Try update app and repeat")]
    
    // check parameters when was error from dataProvider
    func checkErrorFromDataProvider() {
        XCTAssertEqual(defaultsProvider.readHits, 1)
        XCTAssertEqual(dataProvider.getClassesHits, 1)
        XCTAssertEqual(dataProvider.getCardsHits, 0)
        XCTAssertEqual(subject.rowsCount, 0)
        XCTAssertEqual(subject.classButtonTitle, defaultClassButtonTitle)
        XCTAssertEqual(defaultsProvider.writeHits, 0)
        XCTAssertNil(defaultsProvider.currentClassName)
        XCTAssertFalse(subject.isLoadingSuccess)
    }
    
    // check parameters when was success reading from data provider
    func checkSuccessFromDataProvider() {
        XCTAssertEqual(defaultsProvider.readHits, 1)
        XCTAssertEqual(dataProvider.getClassesHits, 1)
        XCTAssertEqual(dataProvider.getCardsHits, 1)
        XCTAssertEqual(subject.rowsCount, cards.count)
        XCTAssertEqual(subject.classButtonTitle, hsClasses.first?.name)
        XCTAssertEqual(defaultsProvider.currentClassName, hsClasses.first?.name)
        XCTAssertTrue(subject.isLoadingSuccess)
    }
    
    // test case: defaults have value for current class and hsClassNames not empty
    func testReadFromDefaults() {
        
        defaultsProvider.currentClassName = "Warrior"
        
        dataProvider.cardsResult = Result({ return cards })
        let expectSuccess = expectation(description: "Success")
        dataProvider.getCardsFullfillExpectation = {
            expectSuccess.fulfill()
        }
        
        subject.hsClassNames = hsClassNames
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        XCTAssertEqual(subject.classButtonTitle, "Warrior")
        XCTAssertEqual(defaultsProvider.readHits, 1)
        XCTAssertTrue(subject.isLoadingSuccess)
        
        XCTAssertEqual(dataProvider.getCardsHits, 1)
        XCTAssertEqual(subject.rowsCount, cards.count)
    }
    
    // test case: defaults have value for current class and hsClassNames is empty
    func testReadFromDefaultEmptyNames() {
        defaultsProvider.currentClassName = "Warrior"
        
        let expectGetClassesSuccess = expectation(description: "Success")
        let expectGetCardsSuccess = expectation(description: "Success")
        
        dataProvider.result = Result({ return hsClasses })
        dataProvider.cardsResult = Result({ return cards })
        
        dataProvider.getClassesFullfillExpectation = {
            expectGetClassesSuccess.fulfill()
        }
        dataProvider.getCardsFullfillExpectation = {
            expectGetCardsSuccess.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkSuccessFromDataProvider()
        XCTAssertEqual(defaultsProvider.writeHits, 2)
    }
    
    // test case: no data in default, success reading data from dataProvider
    func testSuccessReadFromDataProvider() {
        let expectGetClassesSuccess = expectation(description: "Success")
        let expectGetCardsSuccess = expectation(description: "Success")
        
        dataProvider.result = Result({ return hsClasses })
        dataProvider.cardsResult = Result({ return cards })

        dataProvider.getClassesFullfillExpectation = {
            expectGetClassesSuccess.fulfill()
        }
        dataProvider.getCardsFullfillExpectation = {
            expectGetCardsSuccess.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkSuccessFromDataProvider()
        XCTAssertEqual(defaultsProvider.writeHits, 1)
    }
    
    // test case: success reading from data provider with empty array in response
    func testSuccessReadingWithEmptyArray() {
        let expectSuccess = expectation(description: "Success")
        
        dataProvider.result = Result({ return [HSClass]() })
        dataProvider.getClassesFullfillExpectation = {
            expectSuccess.fulfill()
        }

        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        XCTAssertEqual(defaultsProvider.readHits, 1)
        XCTAssertEqual(dataProvider.getClassesHits, 1)
        XCTAssertEqual(dataProvider.getCardsHits, 0)
        XCTAssertEqual(subject.rowsCount, 0)
        XCTAssertEqual(subject.classButtonTitle, defaultClassButtonTitle)
        XCTAssertEqual(defaultsProvider.writeHits, 0)
        XCTAssertNil(defaultsProvider.currentClassName)
        XCTAssertTrue(subject.isLoadingSuccess)
    }
    
    // test case: no data in default, read data from data provider with responseInvalid error
    func testReadFromDataProviderWithResponseInvalid() {
        let expectError = expectation(description: "Error")
        let emptyError = LoadingError.responseInvalid
        
        dataProvider.result = Result({ throw emptyError })
        dataProvider.getClassesFullfillExpectation = {
            expectError.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkErrorFromDataProvider()
        XCTAssertEqual(subject.errorData.title, errorData[0].title)
        XCTAssertEqual(subject.errorData.msg, errorData[0].msg)
    }
    
    // test case: no data in default, read data from data provider with responseStatus error
    func testReadFromDataProviderWithResponseStatus() {
        let expectError = expectation(description: "Error")
        let emptyError = LoadingError.responseStatus(0)
        
        dataProvider.result = Result({ throw emptyError })
        dataProvider.getClassesFullfillExpectation = {
            expectError.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkErrorFromDataProvider()
        XCTAssertEqual(subject.errorData.title, errorData[0].title)
        XCTAssertEqual(subject.errorData.msg, errorData[0].msg)
    }
    
    // test case: no data in default, read data from data provider with requestError error
    func testReadFromDataProviderWithRequestError() {
        let expectError = expectation(description: "Error")
        let emptyError = LoadingError.requestError("")
        
        dataProvider.result = Result({ throw emptyError })
        dataProvider.getClassesFullfillExpectation = {
            expectError.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkErrorFromDataProvider()
        XCTAssertEqual(subject.errorData.title, errorData[1].title)
        XCTAssertEqual(subject.errorData.msg, errorData[1].msg)
    }
    
    // test case: no data in default, read data from data provider with dataEmpty error
    func testReadFromDataProviderWithDataEmpty() {
        let expectError = expectation(description: "Error")
        let emptyError = LoadingError.dataEmpty
        
        dataProvider.result = Result({ throw emptyError })
        dataProvider.getClassesFullfillExpectation = {
            expectError.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkErrorFromDataProvider()
        XCTAssertEqual(subject.errorData.title, errorData[2].title)
        XCTAssertEqual(subject.errorData.msg, errorData[2].msg)
    }
    
    // test case: no data in default, read data from data provider with dataInvalid error
    func testReadFromDataProviderWithDataInvalid() {
        let expectError = expectation(description: "Error")
        let emptyError = LoadingError.invalidData
        
        dataProvider.result = Result({ throw emptyError })
        dataProvider.getClassesFullfillExpectation = {
            expectError.fulfill()
        }
        
        subject.updateData()
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkErrorFromDataProvider()
        XCTAssertEqual(subject.errorData.title, errorData[2].title)
        XCTAssertEqual(subject.errorData.msg, errorData[2].msg)
    }
}
