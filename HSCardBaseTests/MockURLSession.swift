//
//  MockURLSession.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 06/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

class MockURLSession: SessionProtocol {
    var data: Data! = nil
    var response: URLResponse! = nil
    var error: Error! = nil
    
    private(set) var getDataHits: Int = 0
    private(set) var getDataLastRequest: URLRequest! = nil
    
    // MARK: SessionProtocol
    func getData(with request: URLRequest, completion: @escaping DataTaskResult) {
        getDataHits += 1
        getDataLastRequest = request
        
        // Mock asynchronous completion call
        DispatchQueue.main.async { [weak self] in
            completion(self?.data, self?.response, self?.error)
        }
    }
}
