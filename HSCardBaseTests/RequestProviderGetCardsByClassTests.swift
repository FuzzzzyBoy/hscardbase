//
//  RequestProviderGetCardsByClassTests.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 16/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

// swiftlint:disable force_cast

class RequestProviderGetCardsByClassTests: XCTestCase {
    
    var subject: RequestProvider!
    var session: MockURLSession!
    
    let timeout = 5.0
    let url = "https://www.google.ru"
    let successCode = 200
    
    let className = "Warrior"
    let jsonData: CardsData = [
    [
        "cardId": "AT_071e",
        "name": "Alexstrasza's Boon"
    ],
    [
        "cardId": "GVG_086e",
        "name": "Armor Plated"
    ],
    [
        "cardId": "EX1_604o",
        "name": "Berserk"
    ]]
    
    let endpoint = "https://omgvamp-hearthstone-v1.p.mashape.com/"
    let header = (field: "X-Mashape-Key", value: "QmjlAjOjNwmshuH5cXVicxWjM627p1FSosljsnRBZALaukcOm3")
    
    let infoPath = "cards/classes/Warrior"
    
    override func setUp() {
        super.setUp()
        session = MockURLSession()
        subject = RequestProvider(session: session)
    }
    
    func checkDependency(on session: MockURLSession, with path: String) {
        XCTAssertEqual(session.getDataHits, 1)
        let request = session.getDataLastRequest
        XCTAssertNotNil(request)
        print(request!.url!.absoluteString)
        print(endpoint + path)
        XCTAssertEqual(request?.url?.absoluteString, endpoint + path)
        let headerFields = request?.allHTTPHeaderFields
        XCTAssertNotNil(headerFields)
        XCTAssertEqual(headerFields?.count, 1)
        XCTAssertEqual(header.value, headerFields?[header.field])
    }
    
    // server return error, it happens if there is no responce from the server
    func testErrorFromServer() {
        let expectError = expectation(description: "Error during request")
        let error = NSError(domain: "network", code: -1, userInfo: nil)
        self.session.error = error
        
        var actualError: Error!
        
        subject.getCards(byClassName: className) { result in
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
            
            expectError.fulfill()
        }
        
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.requestError(""))
    }
    
    // test invalid response, it hardly could happen but anyway
    func testInvalidResponse() {
        let expectError = expectation(description: "Error in response")
        let response = URLResponse()
        self.session.response = response
        
        var actualError: Error!
        
        subject.getCards(byClassName: className) { result in
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
            
            expectError.fulfill()
        }
        
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.responseInvalid)
    }
    
    // test empty data
    func testEmptyData() {
        let expectError = expectation(description: "Empty data")
        self.session.data = nil
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: self.successCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var actualError: Error!
        
        subject.getCards(byClassName: className) { result in
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
            
            expectError.fulfill()
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.dataEmpty)
    }
    
    // test invalid data
    func testInvalidData() {
        let expectError = expectation(description: "Invalid data")
        let data = "{1}".data(using: String.Encoding.utf8)
        self.session.data = data
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: self.successCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var actualError: Error!
        
        subject.getCards(byClassName: className) { result in
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
            
            expectError.fulfill()
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.invalidData)
    }
    
    // test the case when HTTPURLResponse.statusCode != 2xx
    func testUnsuccessfulResponse() {
        let expectError = expectation(description: "Status code != 2xx")
        let data = try? JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
        self.session.data = data
        let statusCode = 400
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: statusCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var actualError: Error!
        
        subject.getCards(byClassName: className) { result in
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
            
            expectError.fulfill()
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.responseStatus(statusCode))
    }
    
    // test valid data
    func testValidData() {
        let expectSuccess = expectation(description: "Success")
        let data = try? JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
        self.session.data = data
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: self.successCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var value: CardsData!
        
        subject.getCards(byClassName: className) { result in
            XCTAssertNotNil(result.value)
            XCTAssertNil(result.error)
            value = result.value
            
            expectSuccess.fulfill()
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(value.first)
        XCTAssertEqual(value.first! as! [String: String], jsonData.first! as! [String : String])
    }

}
