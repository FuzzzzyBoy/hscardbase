//
//  DataProviderTests.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 05/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

// swiftlint:disable force_cast

class DataProviderTests: XCTestCase {
    
    var subject: DataProvider!
    var requestProvider: MockRequestProvider!
    
    let timeout = 5.0
    
    let validJsonData: InfoData = ["classes": ["Mage", "Warrior"]]
    let invalidJsonData: InfoData = ["1": ["1"]]
    
    let hsClasses = [HSClass(name: "Mage"),
                     HSClass(name: "Warrior")]
    
    override func setUp() {
        super.setUp()
        requestProvider = MockRequestProvider()
        subject = DataProvider(requestProvider: requestProvider)
    }
    
    func checkDependency(on requestProvider: MockRequestProvider) {
        XCTAssertEqual(requestProvider.getInfoHits, 1)
    }
    
    // test DataProvider get classes with valid data
    func testGetClassesWithValidData() {
        let expectSuccess = expectation(description: "Success")
        
        let result = Result({ return validJsonData })
        self.requestProvider.result = result
        
        var value: Classes!
        
        subject.getClasses { result in
            expectSuccess.fulfill()
            
            XCTAssertNotNil(result.value)
            XCTAssertNil(result.error)
            value = result.value
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.requestProvider)
        
        XCTAssertNotNil(value)
        XCTAssertEqual(value, self.hsClasses)
        
    }
    
    // test DataProvider get classes with invalid data
    func testGetClassesWithInvalidData() {
        let expectError = expectation(description: "Invalid data")
        
        let result = Result({ return invalidJsonData})
        self.requestProvider.result = result
        
        var actualError: Error!
        
        subject.getClasses { result in
            expectError.fulfill()
            
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.requestProvider)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.invalidData)
    }
    
    // test use data from previous request
    func testGetClassesWithPreviousrequest() {
        var expect = expectation(description: "Firts run")
        
        let result = Result({ return validJsonData})
        self.requestProvider.result = result
        
        // First run
        var value1: Classes!
        
        subject.getClasses { result in
            expect.fulfill()
            
            XCTAssertNotNil(result.value)
            XCTAssertNil(result.error)
            value1 = result.value
        }
        waitForExpectations(timeout: self.timeout) { _ in }

        checkDependency(on: self.requestProvider)
        
        XCTAssertNotNil(value1)
        XCTAssertEqual(value1, self.hsClasses)
        
        // Second run
        expect = expectation(description: "Second run")
        var value2: Classes!
        
        subject.getClasses { result in
            expect.fulfill()
            
            XCTAssertNotNil(result.value)
            XCTAssertNil(result.error)
            value2 = result.value
        }
        waitForExpectations(timeout: self.timeout) { _ in }

        // Number of getInfo hits does not change because the cached value has been returned
        XCTAssertEqual(requestProvider.getInfoHits, 1)
        
        XCTAssertNotNil(value2)
        XCTAssertEqual(value2, self.hsClasses)
        
        XCTAssertEqual(value1, value2)
    }
}
