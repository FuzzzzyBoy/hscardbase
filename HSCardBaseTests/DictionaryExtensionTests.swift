//
//  DictionaryExtensionTests.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 07/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

class DictionaryExtensionTests: XCTestCase {
    
    // test isEqual with equal dictionaries
    func testisEqualWithEqualDict() {
        let lhs = ["11": ["one", "two"], "35": ["three", "five"]]
        let rhs = lhs
        
        XCTAssert(lhs.isEqual(rhs))
    }
    
    // test isEqual with different values
    func testisEqualWithDifferentValues() {
        let lhs = ["11": ["one", "two"], "35": ["three", "five"]]
        let rhs = ["11": ["one", "two"], "35": ["five", "five"]]
        
        XCTAssertFalse(lhs.isEqual(rhs))
    }
    
    // test isEqual with different keys
    func testisEqualWithDifferentKeys() {
        let lhs = ["11": ["one", "two"], "35": ["three", "five"]]
        let rhs = ["11": ["one", "two"], "33": ["three", "five"]]
        
        XCTAssertFalse(lhs.isEqual(rhs))
    }
}
