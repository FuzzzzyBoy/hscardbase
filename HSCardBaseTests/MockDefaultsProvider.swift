//
//  MockDefaultsProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

private enum Keys {
    static let currentClassName = "currentClassName"
}

class MockDefaultsProvider: DefaultsProviderProtocol {
    
    private(set) var writeHits: Int = 0
    private(set) var readHits: Int = 0
    
    var currentClassName: String? {
        get {
            readHits += 1
            return data[Keys.currentClassName]
        }
        
        set {
            writeHits += 1
            data[Keys.currentClassName] = newValue
        }
    }
    
    // MARK: - Dictionary for store defaults
    var data = [String: String]()
}
