//
//  DefaultsProviderTests.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

class DefaultsProviderTests: XCTestCase {
    
    var subject: DefaultsProvider!
    var defaults: MockDefaults!
    
    var currentClassName = "Mage"
    var keyCurrentClassName = "currentClassName"
    
    override func setUp() {
        super.setUp()
        
        defaults = MockDefaults()
        subject = DefaultsProvider(defaults: defaults)
    }
    
    func testWriteCurrentClassName() {
        subject.currentClassName = currentClassName
        
        XCTAssertEqual(defaults.writeHits, 1)
        XCTAssertNotNil(defaults.data[keyCurrentClassName])
        
        let value = defaults.data[keyCurrentClassName] as? String
        XCTAssertNotNil(value)
        XCTAssertEqual(value, currentClassName)
    }
    
    func testReadCurrentClassName() {
        let value: String? = subject.currentClassName
        
        XCTAssertEqual(defaults.readHits, 1)
        XCTAssertNil(value)
    }
    
    func testWriteReadCurrentClassName() {
        subject.currentClassName = currentClassName
        
        XCTAssertEqual(defaults.writeHits, 1)
        XCTAssertNotNil(defaults.data[keyCurrentClassName])
        
        var value = defaults.data[keyCurrentClassName] as? String
        XCTAssertNotNil(value)
        XCTAssertEqual(value, currentClassName)
        
        value = subject.currentClassName
        
        XCTAssertEqual(defaults.readHits, 1)
        XCTAssertNotNil(value)
        XCTAssertEqual(value!, currentClassName)
    }
}
