//
//  MockRequestProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 06/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

class MockRequestProvider: RequestProviderProtocol {
    
    var result: RequestResult<InfoData>!
    var cardsResult: RequestCardsResult!
    
    private(set) var getInfoHits: Int = 0
    private(set) var getCardsHits = 0
    
    func getInfo(completion: @escaping (RequestResult<InfoData>) -> Void) {
        getInfoHits += 1
        
        // Mock asynchronous completion call
        DispatchQueue.main.async { [weak self] in
            completion(self!.result)
        }
    }
    
    func getCards(byClassName className: String,
                  completion: @escaping (RequestCardsResult) -> Void) {
        getCardsHits += 1
        
        // Mock asynchronous completion call
        DispatchQueue.main.async { [weak self] in
            completion(self!.cardsResult)
        }
    }
}
