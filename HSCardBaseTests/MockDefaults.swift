//
//  MockDefaultsProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

class MockDefaults: DefaultsProtocol {
    
    // MARK: - Dictionary to store defaults
    var data = [String: Any]()
    
    private(set) var readHits: Int = 0
    private(set) var writeHits: Int = 0
    
    // MARK: - DefaultsProviderProtocol
    func set(_ value: Any?, forKey defaultName: String) {
        writeHits += 1
        data[defaultName] = value
    }
    
    func string(forKey defaultName: String) -> String? {
        readHits += 1
        if let value = data[defaultName] as? String {
            return value
        } else {
            return nil
        }
    }
}
