//
//  MockDataProvider.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 08/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import Foundation

class MockDataProvider: DataProviderProtocol {
    
    var result: Result<Classes>!
    var cardsResult: Result<Cards>!
    var getClassesFullfillExpectation: (() -> Void)!
    var getCardsFullfillExpectation: (() -> Void)!
    
    private(set) var getClassesHits = 0
    private(set) var getCardsHits = 0
    
    func getClasses(completion: @escaping (Result<Classes>) -> Void) {
        getClassesHits += 1
        
        // Mock asynchronous completion call
        DispatchQueue.main.async { [unowned self] in
            self.getClassesFullfillExpectation()
            completion(self.result)
        }
    }
    
    func getCards(byClassName className: String,
                  completion: @escaping (Result<Cards>) -> Void) {
        getCardsHits += 1
        
        // Mock asynchronous completion call
        DispatchQueue.main.async { [unowned self] in
            self.getCardsFullfillExpectation()
            completion(self.cardsResult)
        }
    }
}
