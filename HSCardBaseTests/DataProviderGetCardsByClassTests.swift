//
//  DataProviderGetCardsByClassTests.swift
//  HSCardBase
//
//  Created by Кирилл Шаханский on 16/06/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

// swiftlint:disable force_cast

class DataProviderGetCardsByClassTests: XCTestCase {
    
    var subject: DataProvider!
    var requestProvider: MockRequestProvider!
    
    let timeout = 5.0
    
    let className = "Warrior"
    
    let validJsonData: [[String: Any]] = [
        [
            "cardId": "AT_071e",
            "name": "Alexstrasza's Boon",
            "text": "+1 Attack and <b>Charge</b>."
        ],
        [
            "cardId": "GVG_086e",
            "name": "Armor Plated",
            "text": "Increased Attack."
        ],
        [
            "cardId": "GVG_056t",
            "name": "Burrowing Mine",
            "text": "When you draw this, it explodes. You take 10 damage and draw a card.",
            "img": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/GVG_056t.png"
        ]]
    
    lazy var cards: Cards = {
        var resultArray = Cards()
        for data in self.validJsonData {
            if let card = HSCard(parameters: data) {
                resultArray.append(card)
            }
        }
        return resultArray
    }()

    let invalidJsonData: [[String: Any]] = [["": ""], ["": ""]]
    
    override func setUp() {
        super.setUp()
        requestProvider = MockRequestProvider()
        subject = DataProvider(requestProvider: requestProvider)
    }
    
    func checkDependency(on requestProvider: MockRequestProvider) {
        XCTAssertEqual(requestProvider.getCardsHits, 1)
    }
    
    // test DataProvider get cards with valid data
    func testGetClassesWithValidData() {
        let expectSuccess = expectation(description: "Success")
        
        let result = Result({ return validJsonData })
        self.requestProvider.cardsResult = result
        
        var value: Cards!
        
        subject.getCards(byClassName: className) { result in
            expectSuccess.fulfill()
            
            XCTAssertNotNil(result.value)
            XCTAssertNil(result.error)
            value = result.value
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.requestProvider)
        
        XCTAssertNotNil(value)
        XCTAssertEqual(value, cards)
    }
    
    // test DataProvider get classes with invalid data
    func testGetClassesWithInvalidData() {
        let expectError = expectation(description: "Invalid data")
        
        let result = Result({ return invalidJsonData})
        self.requestProvider.cardsResult = result
        
        var actualError: Error!
        
        subject.getCards(byClassName: className) { result in
            print("\n 1111 \n")
            expectError.fulfill()
            
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.requestProvider)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.invalidData)
    }

}
