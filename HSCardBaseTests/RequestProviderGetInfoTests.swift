//
//  RequestProviderGetInfoTests.swift
//  HSCardBase
//
//  Created by Alexey Afanasyev on 31/05/2017.
//  Copyright © 2017 Кирилл Шаханский. All rights reserved.
//

import XCTest

// swiftlint:disable force_cast

class RequestProviderGetInfoTests: XCTestCase {
    
    var subject: RequestProvider!
    var session: MockURLSession!
    
    let timeout = 10.0 // I set timeout to 5 minutes to be able to debug
    let url = "https://www.google.ru"
    let successCode = 200
    let jsonData: ResponseData = ["classes": ["Mage", "Warrior"]]

    // We have to repeat this info here as it is private implementation of RequestProvider
    // but we know it as we have just written the provider and want to test it.
    let endpoint = "https://omgvamp-hearthstone-v1.p.mashape.com/"
    let headers = ["X-Mashape-Key": "QmjlAjOjNwmshuH5cXVicxWjM627p1FSosljsnRBZALaukcOm3",
                                  "Accept": "application/json"]
    let infoPath = "info"

    override func setUp() {
        super.setUp()
        session = MockURLSession()
        subject = RequestProvider(session: session)
    }
 
    func checkDependency(on session: MockURLSession, with path: String) {
        XCTAssertEqual(session.getDataHits, 1)
        let request = session.getDataLastRequest
        XCTAssertNotNil(request)
        XCTAssertEqual(request?.url?.absoluteString, endpoint + path)
        let headerFields = request?.allHTTPHeaderFields
        XCTAssertNotNil(headerFields)
        XCTAssertEqual(headerFields?.count, 2)
        for key in headers.keys {
            XCTAssertEqual(headers[key], headerFields?[key])
        }
    }
    
    // Integration test, might fail if there is no internet connection
    func postpone_testGetInfo() {
        let expectInfo = expectation(description: "Request Provider returns Info structure")
        let requestProvider = RequestProvider()
        requestProvider.getInfo(completion: { (result) in
            do {
                let json = try result.unwrap()
                XCTAssertNotNil(json)
                expectInfo.fulfill()
            } catch {
                XCTFail()
            }
        })
        
        waitForExpectations(timeout: self.timeout) { (error) in
            // Anyway an error might happen if you stay in debugger more then 5 minutes
            XCTAssertNil(error)
        }
    }

    // server return error, it happens if there is no responce from the server
    func testErrorFromServer() {
        let expectError = expectation(description: "Error during request")
        let error = NSError(domain: "network", code: -1, userInfo: nil)
        self.session.error = error
        
        var actualError: Error!
        
        subject.getInfo { result in
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
            
            expectError.fulfill()
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)

        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.requestError(""))
    }
    
    // test invalid response, it hardly could happen but anyway
    func testInvalidResponse() {
        let expectError = expectation(description: "Error in response")
        let response = URLResponse()
        self.session.response = response
        
        var actualError: Error!
        
        subject.getInfo { result in
            expectError.fulfill()
            
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.responseInvalid)
    }
    
    // test empty data
    func testEmptyData() {
        let expectError = expectation(description: "Empty data")
        self.session.data = nil
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: self.successCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var actualError: Error!
        
        subject.getInfo { result in
            expectError.fulfill()
            
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.dataEmpty)
    }
    
    // test invalid data
    func testInvalidData() {
        let expectError = expectation(description: "Invalid data")
        let data = "{1}".data(using: String.Encoding.utf8)
        self.session.data = data
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: self.successCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var actualError: Error!
        
        subject.getInfo { result in
            expectError.fulfill()
            
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.invalidData)
    }
    
    // test valid data
    func testValidData() {
        let expectSuccess = expectation(description: "Success")
        let data = try? JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
        self.session.data = data
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: self.successCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var value: ResponseData!
        
        subject.getInfo { result in
            expectSuccess.fulfill()
            
            XCTAssertNotNil(result.value)
            XCTAssertNil(result.error)
            value = result.value
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(value)
        XCTAssert((jsonData as! InfoData).isEqual(value as! InfoData))
    }

    // test the case when HTTPURLResponse.statusCode != 2xx
    func testUnsuccessfulResponse() {
        let expectError = expectation(description: "Status code != 2xx")
        let data = try? JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
        self.session.data = data
        let statusCode = 400
        let response = HTTPURLResponse(url: URL(string: self.url)!,
                                       statusCode: statusCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        self.session.response = response
        
        var actualError: Error!
        
        subject.getInfo { result in
            expectError.fulfill()
            
            XCTAssertNil(result.value)
            XCTAssertNotNil(result.error)
            actualError = result.error
        }
        waitForExpectations(timeout: self.timeout) { _ in }
        
        checkDependency(on: self.session, with: infoPath)
        
        XCTAssertNotNil(actualError)
        XCTAssertEqual(actualError as! LoadingError, LoadingError.responseStatus(statusCode))
    }
}
